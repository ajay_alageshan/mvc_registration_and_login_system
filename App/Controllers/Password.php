<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;

class Password extends \Core\Controller
{
  public function forgotAction()
  {
    View::renderTemplate("Password/forgot.html");
  }
  public function requestResetAction()
  {
      User::sendPasswordReset($_POST["email"]);
      View::renderTemplate("Password/reset_requested.html");
  }
  public function resetAction()//76. creating a route for the token
  {
    $token = $this->route_params['token'];
    
    $user = $this->getUserOrExit($token);

    View::renderTemplate('Password/reset.html',['token'=>$token]);
    
  }
  //80 
  public function resetPasswordAction()
  {
      $token = $_POST['token'];

      $user = $this->getUserOrExit($token);

      // echo "reset user password here.";

      // echo $_POST['password'];

      if($user->resetPassword($_POST['password']))
      {
        View::renderTemplate('Password/reset_success.html');
      }
      else
      {
        View::renderTemplate('Password/reset.html',[
          'token'=>$token,
          'user'=>$user]);
      }
  }
  public function getUserOrExit($token)
  {
    $user = User::findByPasswordReset($token);

    if($user)
    {
      return $user;
    }
    else
    {
      View::renderTemplate('Password/token_expired.html');
      exit;
    }
  }
}